#!/bin/sh

BASE_REL=$(dpkg-parsechangelog 2>/dev/null | sed -ne 's/Version: \([0-9]\)~.*/\1/p')
OLDDIR=${PWD}
GOS_DIR=${OLDDIR}/get-orig-source
SVN_COMMIT='svn log http://svn.drobilla.net/pugl/trunk/pugl/ -l 1 | sed -ne "s/r\([0-9]\+\).*/\1/p"'

if [ -z ${BASE_REL} ]; then
	echo 'Please run this script from the sources root directory.'
	exit 1
fi

UNPACK_WAF=$PWD/debian/unpack_waf.sh

rm -rf ${GOS_DIR}
mkdir ${GOS_DIR} && cd ${GOS_DIR}
PUGL_SVN_COMMIT=$(eval "${SVN_COMMIT}")
svn export -r ${PUGL_SVN_COMMIT} http://svn.drobilla.net/pugl/trunk/ pugl
cd pugl/
${UNPACK_WAF}
cd .. && tar cjf \
	${OLDDIR}/pugl_${BASE_REL}~svn${PUGL_SVN_COMMIT}.orig.tar.bz2 \
	pugl --exclude-vcs
rm -rf ${GOS_DIR}
